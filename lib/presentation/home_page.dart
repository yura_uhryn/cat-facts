import 'package:cat_facts/providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Consumer(
            builder: (context, watch, child) {
              final state = watch(catFactsNotifierProvider.state);
              return state.map(
                initial: (_) => Container(),
                loading: (_) => CircularProgressIndicator(),
                error: (_) => Text(
                  _.message,
                  style: TextStyle(color: Colors.red),
                ),
                showFact: (state) => Expanded(
                  child: ListView.builder(
                    itemCount: state.facts.length,
                    itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ListTile(
                        tileColor: Colors.green.withOpacity(0.2),
                        title: Text(state.facts[index].text),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
          RaisedButton(
            onPressed: () => context.read(catFactsNotifierProvider).getFact(),
            child: Text('New fact'),
          ),
        ],
      ),
    );
  }
}
