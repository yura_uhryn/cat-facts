part of 'cat_facts_notifier.dart';

@freezed
abstract class CatFactsEvent with _$CatFactsEvent {
  const factory CatFactsEvent.started() = _Started;
  const factory CatFactsEvent.loadNew() = _LoadNew;
}
