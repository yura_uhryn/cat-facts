part of 'cat_facts_notifier.dart';

@freezed
abstract class CatFactsState with _$CatFactsState {
  const factory CatFactsState.initial() = _Initial;
  const factory CatFactsState.loading() = _Loading;
  const factory CatFactsState.error(String message) = _Error;
  const factory CatFactsState.showFact(BuiltList<Fact> facts) = _ShowFact;
}
