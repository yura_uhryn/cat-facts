import 'package:cat_facts/domain/model/fact.dart';
import 'package:cat_facts/domain/service/api_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:built_collection/built_collection.dart';

part 'cat_facts_event.dart';
part 'cat_facts_state.dart';

part 'cat_facts_notifier.freezed.dart';

@injectable
class CatFactsNotifier extends StateNotifier<CatFactsState> {
  final CatFactsService _service;
  CatFactsNotifier(this._service) : super(_Initial());

  Future<void> getFact() async {
    state = _Loading();
    final factsResponse = await _service.getFacts();
    final facts = factsResponse.body;
    state = _ShowFact(facts);
  }
}
