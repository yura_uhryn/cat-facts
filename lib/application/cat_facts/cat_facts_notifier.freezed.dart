// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'cat_facts_notifier.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CatFactsEventTearOff {
  const _$CatFactsEventTearOff();

// ignore: unused_element
  _Started started() {
    return const _Started();
  }

// ignore: unused_element
  _LoadNew loadNew() {
    return const _LoadNew();
  }
}

/// @nodoc
// ignore: unused_element
const $CatFactsEvent = _$CatFactsEventTearOff();

/// @nodoc
mixin _$CatFactsEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result started(),
    @required Result loadNew(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result started(),
    Result loadNew(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result started(_Started value),
    @required Result loadNew(_LoadNew value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result started(_Started value),
    Result loadNew(_LoadNew value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $CatFactsEventCopyWith<$Res> {
  factory $CatFactsEventCopyWith(
          CatFactsEvent value, $Res Function(CatFactsEvent) then) =
      _$CatFactsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$CatFactsEventCopyWithImpl<$Res>
    implements $CatFactsEventCopyWith<$Res> {
  _$CatFactsEventCopyWithImpl(this._value, this._then);

  final CatFactsEvent _value;
  // ignore: unused_field
  final $Res Function(CatFactsEvent) _then;
}

/// @nodoc
abstract class _$StartedCopyWith<$Res> {
  factory _$StartedCopyWith(_Started value, $Res Function(_Started) then) =
      __$StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$StartedCopyWithImpl<$Res> extends _$CatFactsEventCopyWithImpl<$Res>
    implements _$StartedCopyWith<$Res> {
  __$StartedCopyWithImpl(_Started _value, $Res Function(_Started) _then)
      : super(_value, (v) => _then(v as _Started));

  @override
  _Started get _value => super._value as _Started;
}

/// @nodoc
class _$_Started implements _Started {
  const _$_Started();

  @override
  String toString() {
    return 'CatFactsEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result started(),
    @required Result loadNew(),
  }) {
    assert(started != null);
    assert(loadNew != null);
    return started();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result started(),
    Result loadNew(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result started(_Started value),
    @required Result loadNew(_LoadNew value),
  }) {
    assert(started != null);
    assert(loadNew != null);
    return started(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result started(_Started value),
    Result loadNew(_LoadNew value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements CatFactsEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$LoadNewCopyWith<$Res> {
  factory _$LoadNewCopyWith(_LoadNew value, $Res Function(_LoadNew) then) =
      __$LoadNewCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadNewCopyWithImpl<$Res> extends _$CatFactsEventCopyWithImpl<$Res>
    implements _$LoadNewCopyWith<$Res> {
  __$LoadNewCopyWithImpl(_LoadNew _value, $Res Function(_LoadNew) _then)
      : super(_value, (v) => _then(v as _LoadNew));

  @override
  _LoadNew get _value => super._value as _LoadNew;
}

/// @nodoc
class _$_LoadNew implements _LoadNew {
  const _$_LoadNew();

  @override
  String toString() {
    return 'CatFactsEvent.loadNew()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadNew);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result started(),
    @required Result loadNew(),
  }) {
    assert(started != null);
    assert(loadNew != null);
    return loadNew();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result started(),
    Result loadNew(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadNew != null) {
      return loadNew();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result started(_Started value),
    @required Result loadNew(_LoadNew value),
  }) {
    assert(started != null);
    assert(loadNew != null);
    return loadNew(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result started(_Started value),
    Result loadNew(_LoadNew value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadNew != null) {
      return loadNew(this);
    }
    return orElse();
  }
}

abstract class _LoadNew implements CatFactsEvent {
  const factory _LoadNew() = _$_LoadNew;
}

/// @nodoc
class _$CatFactsStateTearOff {
  const _$CatFactsStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return const _Initial();
  }

// ignore: unused_element
  _Loading loading() {
    return const _Loading();
  }

// ignore: unused_element
  _Error error(String message) {
    return _Error(
      message,
    );
  }

// ignore: unused_element
  _ShowFact showFact(BuiltList<Fact> facts) {
    return _ShowFact(
      facts,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CatFactsState = _$CatFactsStateTearOff();

/// @nodoc
mixin _$CatFactsState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result error(String message),
    @required Result showFact(BuiltList<Fact> facts),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result error(String message),
    Result showFact(BuiltList<Fact> facts),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result loading(_Loading value),
    @required Result error(_Error value),
    @required Result showFact(_ShowFact value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result loading(_Loading value),
    Result error(_Error value),
    Result showFact(_ShowFact value),
    @required Result orElse(),
  });
}

/// @nodoc
abstract class $CatFactsStateCopyWith<$Res> {
  factory $CatFactsStateCopyWith(
          CatFactsState value, $Res Function(CatFactsState) then) =
      _$CatFactsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$CatFactsStateCopyWithImpl<$Res>
    implements $CatFactsStateCopyWith<$Res> {
  _$CatFactsStateCopyWithImpl(this._value, this._then);

  final CatFactsState _value;
  // ignore: unused_field
  final $Res Function(CatFactsState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$CatFactsStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'CatFactsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result error(String message),
    @required Result showFact(BuiltList<Fact> facts),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result error(String message),
    Result showFact(BuiltList<Fact> facts),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result loading(_Loading value),
    @required Result error(_Error value),
    @required Result showFact(_ShowFact value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result loading(_Loading value),
    Result error(_Error value),
    Result showFact(_ShowFact value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements CatFactsState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$LoadingCopyWith<$Res> {
  factory _$LoadingCopyWith(_Loading value, $Res Function(_Loading) then) =
      __$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingCopyWithImpl<$Res> extends _$CatFactsStateCopyWithImpl<$Res>
    implements _$LoadingCopyWith<$Res> {
  __$LoadingCopyWithImpl(_Loading _value, $Res Function(_Loading) _then)
      : super(_value, (v) => _then(v as _Loading));

  @override
  _Loading get _value => super._value as _Loading;
}

/// @nodoc
class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'CatFactsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result error(String message),
    @required Result showFact(BuiltList<Fact> facts),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result error(String message),
    Result showFact(BuiltList<Fact> facts),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result loading(_Loading value),
    @required Result error(_Error value),
    @required Result showFact(_ShowFact value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result loading(_Loading value),
    Result error(_Error value),
    Result showFact(_ShowFact value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements CatFactsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$ErrorCopyWith<$Res> {
  factory _$ErrorCopyWith(_Error value, $Res Function(_Error) then) =
      __$ErrorCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$ErrorCopyWithImpl<$Res> extends _$CatFactsStateCopyWithImpl<$Res>
    implements _$ErrorCopyWith<$Res> {
  __$ErrorCopyWithImpl(_Error _value, $Res Function(_Error) _then)
      : super(_value, (v) => _then(v as _Error));

  @override
  _Error get _value => super._value as _Error;

  @override
  $Res call({
    Object message = freezed,
  }) {
    return _then(_Error(
      message == freezed ? _value.message : message as String,
    ));
  }
}

/// @nodoc
class _$_Error implements _Error {
  const _$_Error(this.message) : assert(message != null);

  @override
  final String message;

  @override
  String toString() {
    return 'CatFactsState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Error &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @override
  _$ErrorCopyWith<_Error> get copyWith =>
      __$ErrorCopyWithImpl<_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result error(String message),
    @required Result showFact(BuiltList<Fact> facts),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return error(message);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result error(String message),
    Result showFact(BuiltList<Fact> facts),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result loading(_Loading value),
    @required Result error(_Error value),
    @required Result showFact(_ShowFact value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return error(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result loading(_Loading value),
    Result error(_Error value),
    Result showFact(_ShowFact value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements CatFactsState {
  const factory _Error(String message) = _$_Error;

  String get message;
  _$ErrorCopyWith<_Error> get copyWith;
}

/// @nodoc
abstract class _$ShowFactCopyWith<$Res> {
  factory _$ShowFactCopyWith(_ShowFact value, $Res Function(_ShowFact) then) =
      __$ShowFactCopyWithImpl<$Res>;
  $Res call({BuiltList<Fact> facts});
}

/// @nodoc
class __$ShowFactCopyWithImpl<$Res> extends _$CatFactsStateCopyWithImpl<$Res>
    implements _$ShowFactCopyWith<$Res> {
  __$ShowFactCopyWithImpl(_ShowFact _value, $Res Function(_ShowFact) _then)
      : super(_value, (v) => _then(v as _ShowFact));

  @override
  _ShowFact get _value => super._value as _ShowFact;

  @override
  $Res call({
    Object facts = freezed,
  }) {
    return _then(_ShowFact(
      facts == freezed ? _value.facts : facts as BuiltList<Fact>,
    ));
  }
}

/// @nodoc
class _$_ShowFact implements _ShowFact {
  const _$_ShowFact(this.facts) : assert(facts != null);

  @override
  final BuiltList<Fact> facts;

  @override
  String toString() {
    return 'CatFactsState.showFact(facts: $facts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ShowFact &&
            (identical(other.facts, facts) ||
                const DeepCollectionEquality().equals(other.facts, facts)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(facts);

  @override
  _$ShowFactCopyWith<_ShowFact> get copyWith =>
      __$ShowFactCopyWithImpl<_ShowFact>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result error(String message),
    @required Result showFact(BuiltList<Fact> facts),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return showFact(facts);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result error(String message),
    Result showFact(BuiltList<Fact> facts),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showFact != null) {
      return showFact(facts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result loading(_Loading value),
    @required Result error(_Error value),
    @required Result showFact(_ShowFact value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(error != null);
    assert(showFact != null);
    return showFact(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result loading(_Loading value),
    Result error(_Error value),
    Result showFact(_ShowFact value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (showFact != null) {
      return showFact(this);
    }
    return orElse();
  }
}

abstract class _ShowFact implements CatFactsState {
  const factory _ShowFact(BuiltList<Fact> facts) = _$_ShowFact;

  BuiltList<Fact> get facts;
  _$ShowFactCopyWith<_ShowFact> get copyWith;
}
