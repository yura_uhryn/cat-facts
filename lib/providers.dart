import 'package:cat_facts/domain/service/api_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:cat_facts/application/cat_facts/cat_facts_notifier.dart';

final catFactsServiceProvider = Provider<CatFactsService>(
  (ref) => CatFactsService.create(),
);

final catFactsNotifierProvider = StateNotifierProvider(
  (_) => CatFactsNotifier(_.watch(catFactsServiceProvider)),
);
