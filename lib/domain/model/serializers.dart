import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:cat_facts/domain/model/fact.dart';

part 'serializers.g.dart';

@SerializersFor(const [Fact])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
