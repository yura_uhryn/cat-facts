import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'fact.g.dart';

abstract class Fact implements Built<Fact, FactBuilder> {
  Fact._();

  @nullable
  String get id;
  @nullable
  int get v;
  @nullable
  String get user;
  // Timestamp updateAt,
  // Timestamp updateAt,
  String get text;
  @nullable
  bool get isDeleted;
  @nullable
  String get source;

  // factory Fact.fromJson(Map<String, dynamic> json) => _$FactFromJson(json);

  factory Fact([updates(FactBuilder b)]) = _$Fact;

  static Serializer<Fact> get serializer => _$factSerializer;
}
