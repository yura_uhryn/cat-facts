// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fact.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Fact> _$factSerializer = new _$FactSerializer();

class _$FactSerializer implements StructuredSerializer<Fact> {
  @override
  final Iterable<Type> types = const [Fact, _$Fact];
  @override
  final String wireName = 'Fact';

  @override
  Iterable<Object> serialize(Serializers serializers, Fact object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'text',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
    ];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.v != null) {
      result
        ..add('v')
        ..add(serializers.serialize(object.v,
            specifiedType: const FullType(int)));
    }
    if (object.user != null) {
      result
        ..add('user')
        ..add(serializers.serialize(object.user,
            specifiedType: const FullType(String)));
    }
    if (object.isDeleted != null) {
      result
        ..add('isDeleted')
        ..add(serializers.serialize(object.isDeleted,
            specifiedType: const FullType(bool)));
    }
    if (object.source != null) {
      result
        ..add('source')
        ..add(serializers.serialize(object.source,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Fact deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FactBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'v':
          result.v = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'user':
          result.user = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'text':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'isDeleted':
          result.isDeleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'source':
          result.source = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Fact extends Fact {
  @override
  final String id;
  @override
  final int v;
  @override
  final String user;
  @override
  final String text;
  @override
  final bool isDeleted;
  @override
  final String source;

  factory _$Fact([void Function(FactBuilder) updates]) =>
      (new FactBuilder()..update(updates)).build();

  _$Fact._({this.id, this.v, this.user, this.text, this.isDeleted, this.source})
      : super._() {
    if (text == null) {
      throw new BuiltValueNullFieldError('Fact', 'text');
    }
  }

  @override
  Fact rebuild(void Function(FactBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FactBuilder toBuilder() => new FactBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Fact &&
        id == other.id &&
        v == other.v &&
        user == other.user &&
        text == other.text &&
        isDeleted == other.isDeleted &&
        source == other.source;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, id.hashCode), v.hashCode), user.hashCode),
                text.hashCode),
            isDeleted.hashCode),
        source.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Fact')
          ..add('id', id)
          ..add('v', v)
          ..add('user', user)
          ..add('text', text)
          ..add('isDeleted', isDeleted)
          ..add('source', source))
        .toString();
  }
}

class FactBuilder implements Builder<Fact, FactBuilder> {
  _$Fact _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  int _v;
  int get v => _$this._v;
  set v(int v) => _$this._v = v;

  String _user;
  String get user => _$this._user;
  set user(String user) => _$this._user = user;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  bool _isDeleted;
  bool get isDeleted => _$this._isDeleted;
  set isDeleted(bool isDeleted) => _$this._isDeleted = isDeleted;

  String _source;
  String get source => _$this._source;
  set source(String source) => _$this._source = source;

  FactBuilder();

  FactBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _v = _$v.v;
      _user = _$v.user;
      _text = _$v.text;
      _isDeleted = _$v.isDeleted;
      _source = _$v.source;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Fact other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Fact;
  }

  @override
  void update(void Function(FactBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Fact build() {
    final _$result = _$v ??
        new _$Fact._(
            id: id,
            v: v,
            user: user,
            text: text,
            isDeleted: isDeleted,
            source: source);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
