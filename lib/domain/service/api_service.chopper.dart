// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$CatFactsService extends CatFactsService {
  _$CatFactsService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = CatFactsService;

  Future<Response<BuiltList<Fact>>> getFacts() {
    final $url = 'facts';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<BuiltList<Fact>, Fact>($request);
  }
}
