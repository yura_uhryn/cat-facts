import 'package:cat_facts/domain/converter.dart';
import 'package:cat_facts/domain/model/fact.dart';
import 'package:chopper/chopper.dart';
import 'package:built_collection/built_collection.dart';

part 'api_service.chopper.dart';

@ChopperApi(baseUrl: 'facts')
abstract class CatFactsService extends ChopperService {
  @Get()
  Future<Response<BuiltList<Fact>>> getFacts();

  static CatFactsService create() {
    final client = ChopperClient(
      baseUrl: 'https://cat-fact.herokuapp.com',
      services: [
        _$CatFactsService(),
      ],
      converter: BuiltValueConverter(),
      interceptors: [HttpLoggingInterceptor()],
    );
    return _$CatFactsService(client);
  }
}
